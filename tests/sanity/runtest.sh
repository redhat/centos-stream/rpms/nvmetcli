#!/bin/bash

# Copyright (c) 2016 Red Hat, Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Author: Yi Zhang  <yizhan@redhat.com>

#set -x
source ../include/tc.sh || exit 200

tlog "running $0"

tok "yum -y reinstall nvmetcli"
tok "yum -y remove nvmetcli"
tok "yum -y install nvmetcli"
tok "which nvmetcli"
tok "yum info nvmetcli"
tok "[[ -f "/usr/sbin/nvmetcli" ]]"
tok "modprobe nvmet"
tok "nvmetcli ls"

tend
